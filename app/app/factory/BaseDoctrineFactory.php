<?php

namespace App\Factory;

/**
 * Description of BaseFactory
 *
 * @author David Katolický
 */
abstract class BaseDoctrineFactory extends \Nette\Object implements IFactory {

	
	protected $dao;

	/** @var \Kdyby\Doctrine\EntityManager */
	public $em;

	function __construct($dao, \Kdyby\Doctrine\EntityManager $em) {
		$this->dao = $dao;
		$this->em = $em;
	}

	function create($persist = false) {
		$object = new $this->exceptedClass;
		if ($persist) {
			$this->em->persist($object);
		}
		return $object;
	}

	function createNew($object, $persist) {
		
	}

	function update(&$object, array $values = array(), $autoSave = false) {
		
		if ($object instanceof $this->exceptedClass) {
			foreach ($values as $key => $value) {
				$set = 'set' . ucfirst($key);
				$object->$set($value);
			}
			if ($autoSave) {
				$this->save($object);
			}
			
		} else {
			throw new \Nette\InvalidArgumentException('Excepting instance of ' . $this->exceptedClass . ' but ' . get_class($object) . ' given.');
		}
	}

	function save(&$object) {
		if ($object instanceof $this->exceptedClass) {
			$this->em->persist($object);
			$this->em->flush($object);
		} else {
			throw new \Nette\InvalidArgumentException('Excepting instance of ' . $this->exceptedClass . ' but ' . get_class($object) . ' given.');
		}
	}

	function saveAll() {
		$this->em->flush();
	}

}
