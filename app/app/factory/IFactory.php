<?php

namespace App\Factory;

/**
 * Description of IFactory
 *
 * @author David Katolický
 */
interface IFactory {

	public function create($persist = false);

	public function createNew($object, $persist);

	public function update(&$object, array $values = array(), $autoSave = false);

	public function save(&$object);

	public function saveAll();
}
