<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use App\Factory\UserFactory;
use Nette\Http\Session;
use App\Acl\Acl;

/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator {

	/** @var \App\Factory\UserFactory */
	private $userFactory;

	/** @var Session */
	private $session;

	/** @var Acl */
	private $acl;

	public function __construct(UserFactory $userFactory, Session $session, Acl $acl) {
		$this->userFactory = $userFactory;
		$this->session = $session;
		$this->acl = $acl;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($email, $password) = $credentials;

		$user = $this->userFactory->dao->findOneBy(array('email' => $email));

		if (!$user instanceof \Generated\User) {
			throw new Nette\Security\AuthenticationException('Email není správný.', self::IDENTITY_NOT_FOUND);
		} elseif ($user->active == 0) {
			throw new Nette\Security\AuthenticationException('Uživatel není aktivní. Kontaktujte správce pro aktivaci účtu.', self::NOT_APPROVED);
		} elseif (!Passwords::verify($password, $user->getPass())) {
			$user->attempts = $user->attempts + 1;
			if ($user->attempts >= 10) {
				$user->active = 0;
			}
			$this->userFactory->save($user);
			throw new Nette\Security\AuthenticationException('Bylo zadáno špatné heslo.', self::INVALID_CREDENTIAL);
		} elseif (Passwords::needsRehash($user->getPass())) {
			$user->setPass(Passwords::hash($password));
			$this->userFactory->save($user);
		}

		$attr = $user->toArray();
		unset($attr['pass']);
		if (!$this->session->getSection('availableRoles')->availableRoles) {
			$this->session->getSection('availableRoles')->availableRoles = $this->acl->getAvailableRoles($user->getRole());
		}
		$this->session->getSection('see')->seeAll = false;
		$user->setLastLogon(new Nette\Utils\DateTime);
		$user->setLastHost($_SERVER["REMOTE_ADDR"]);
		$user->setAttempts(0);
		$this->userFactory->save($user);
		return new Nette\Security\Identity($user->getId(), $user->getRole(), $attr);
	}

	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @return \Generated\User
	 */
	public function add($email, $password) {
		$user = $this->userFactory->create();
		$user->setEmail($email);
		$user->setPass(Passwords::hash($password));
		$this->userFactory->save($user);

		return $user;
	}

}
