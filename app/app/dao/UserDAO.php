<?php

namespace App\Dao;

use App\Factory\BaseDoctrineFactory;
use nette;
use Generated\User;
use Nette\Security\Identity;

/**
 * Description of CategoryDAO
 *
 * @author David Katolický
 */
class UserDAO extends BaseDAO {

	public function getNames() {
		$ret = array();
		foreach ($this->findAll() as $user) {
			$ret[$user->getFullName()] = $user->getFullName();
		}
		return $ret;
	}

	/**
	 * Zmena stavu uzivatele
	 * @param int $id_user
	 * @param int $state
	 * @return \Generated\User
	 */
	public function changeState($id_user) {
		$user = $this->find($id_user);

		$user->setActive($user->active == 0 ? 1 : 0);
		$this->save($user);
		return $user;
	}

	public function resetPassword($id) {
		$newPassword = \Nette\Utils\Random::generate();
		return $this->changePassword($id, $newPassword);
	}

	public function changePassword($id, $newPassword) {
		$user = $this->find($id);
		$user->setPass(\Nette\Security\Passwords::hash($newPassword));
		return array($newPassword, $user);
	}

	public function getAllActive() {
		return $this->findBy(array('active' => 1), array('name' => 'asc'));
	}

	public function findActiveByName($name) {
		return $this->createQueryBuilder('user')
										->select('user')
										->where('user.active = :active AND user.name LIKE :name')
										->setParameters(array('active' => 1, 'name' => '%' . $name . '%'))
										->orderBy('user.name')->getQuery()->getResult();
	}


	/**
	 * Vyhleda uzivatele na zaklade jmena a prijmeni
	 * @param string $name
	 * @param string $lastname
	 */
	public function getByName($name, $lastname) {
		return $this->createQueryBuilder('user')
										->select('user')
										->where('user.name LIKE :name OR user.name LIKE :nameReverse')
										->setParameters(array('name' => $name . ' ' . $lastname, 'nameReverse' => $lastname . ' ' . $name))
										->getQuery()->getOneOrNullResult();
	}

	/**
	 * Ziskani roota
	 * @param int $id
	 * @return User
	 */
	public function getRoot($id = null) {
		/**
		 * Nejdříve si získáme uzel, pro který chceme zobrazit děti. Pokud je ID
		 * NULL, pak chceme získat celý strom od "shora"
		 */
		if ($id === null) {
			return $this->findOneBy(array('parent' => null));
		} else {
			return $this->findOneBy(array('id' => $id));
		}
	}

	/**
	 * @param null $id
	 * @return User[]
	 */
	public function getTree($id = null, $order = "user.nleft") {
		$root = $this->getRoot($id);
		return $this->createQueryBuilder('user')
										->select('user')
										->where('user.nleft >= :nleft AND user.nright <= :nright')
										->setParameters(array('nleft' => $root->nleft, 'nright' => $root->nright))
//				->orderBy('user.nleft')
										->orderBy($order)
										->getQuery()->getResult();
	}


	/**
	 * @param null $id int|User
	 * @return array
	 */
	public function getTreeIds($id = NULL) {
		if ($id instanceof User) {
			$root = $id;
		} else if (!$id instanceof Identity) {
			$root = $this->getRoot($id);
		} else {
			$root = $id;
		}
		$ids = [];
		$result = $this->createQueryBuilder('user')
										->select('user.id')
										->where('user.nleft >= :nleft AND user.nright <= :nright')
										->setParameters(array('nleft' => $root->nleft, 'nright' => $root->nright))
										->orderBy('user.nleft')
										->getQuery()->getResult();

		foreach ($result as $children) {
			$ids[] = $children['id'];
		}
		return $ids;
	}

	/**
	 * Pregeneruje strom
	 * @return int
	 */
	public function rebuildTree() {
		$root = $this->findOneBy(array('parent' => null));
		if (!$root) {
			return null;
		}
		// Pošleme parametr $left jako 1, tedy první položku stromu
		$this->rebuildTreeProcess($root, 1);
	}

	/**
	 * Actually rebuild the tree
	 * @param int $parent
	 * @param int $left
	 * @param int $level
	 * @return int
	 */
	private function rebuildTreeProcess($parent, $left, $level = NULL) {
		$right = $left + 1;

		if ($level === NULL) {
			$level = (int) $parent->level;
		}
		$children = $this->findBy(array('parent' => $parent->id), array('id' => 'DESC'));
		foreach ($children as $child) {
			$right = $this->rebuildTreeProcess($child, $right, $level + 1);
		}
		$parent->nleft = $left;
		$parent->nright = $right;
		$parent->level = $level;
		$this->_em->flush();
		return $right + 1;
	}

	public function getUsersByIds(array $ids) {
		if (count($ids) == 0) {
			return null;
		} else {
			$_ids = array();
			$qb = $this->createQueryBuilder('user')
							->select('user.id');
			$qb->add('where', $qb->expr()->orX($qb->expr()->in('user.id', $ids)));
			$agreementIds = $qb->getQuery()->getResult();
			foreach ($agreementIds as $agreementId) {
				$_ids[] = $agreementId['id'];
			}
			return $_ids;
		}
	}

}
