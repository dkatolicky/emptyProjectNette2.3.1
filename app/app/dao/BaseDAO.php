<?php

namespace App\Dao;

/**
 * Description of BaseDAO
 *
 * @author David Katolický
 */
class BaseDAO extends \Kdyby\Doctrine\EntityDao {

	public function __construct($class, \Kdyby\Doctrine\EntityManager $em) {
		parent::__construct($em, new \Doctrine\ORM\Mapping\ClassMetadata($class));
	}

	/**
	 * Vrati data pro selectbox
	 * @param string $key sloupec z db
	 * @param string $value sloupec z db
	 * @param array $data
	 * @return array
	 * @throws Exception\UndefinedVariableException
	 */
	public function getPairs($key = 'id', $value = 'name', $data = null) {
		$return = array();
		if(!$data && !is_array($data)) {
			foreach ($this->findAll() as $item) {
				try {
					$return[$item->$key] = $item->$value;
				} catch (\Exception $ex) {
					throw new Exception\UndefinedVariableException($ex->getMessage());
				}
			}
		} else {
			if(is_array($data)){
				foreach($data as $item) {
					try {
						$return[$item->$key] = $item->$value;
					} catch (\Exception $ex) {
						throw new Exception\UndefinedVariableException($ex->getMessage());
					}
				}
			} else {
				return array();
			}

		}

		
		return $return;
	}
	
}
