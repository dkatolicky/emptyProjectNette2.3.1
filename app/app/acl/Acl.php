<?php

namespace App\Acl;

use Nette\Security\Permission;

/**
 * Description of Acl
 *
 * @author Pavol Bazant
 */
class Acl extends Permission implements \Nette\Security\IAuthorizator {

	public function __construct() {

		$this->addRole('guest');

		$this->allow(self::ALL, self::ALL,self::ALL);

	}

	public function getAvailableRoles($topCategory) {
		$roles = array();
		foreach ($this->getRoles() as $role) {
			if ($role == 'guest') {
				continue;
			}
			$roles[$role] = $this->translateRole($role);
			if ($role == $topCategory) {
				break;
			}
		}
		return $roles;
	}

	public function translateRole($name) {
		switch ($name) {

			default:
				return 'unknown';
		}
	}

}
